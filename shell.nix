let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };

in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.stable.latest.default.override { extensions = [ "rust-src" ]; })

    # Add some extra dependencies from `pkgs`
    pkg-config
    openssl
    linuxPackages.perf
    curl
    gnupg

    jetbrains.idea-community 

    ## to view gpx files
    zombietrackergps # sadly broke maybe fix at some point
    gpxlab

    # useful
    gpsbabel

    # to check out
		gpsbabel-gui
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}
