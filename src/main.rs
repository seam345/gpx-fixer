extern crate gpx;

use std::io::BufReader;
use std::io::BufWriter;
use std::fs::File;
use geo::{GeodesicDistance, Point};

use gpx::{GpxVersion, read, Waypoint, write};
use gpx::{Gpx, Track, TrackSegment};
use geo::point;
use serde_json::map::VacantEntry;
use time::OffsetDateTime;
use time::format_description::well_known::Iso8601;
use std::collections::HashMap;
use std::iter::Skip;
use std::slice::Windows;
use std::iter::Peekable;
use geo::Polygon;
use geo::LineString;
use geo_types::Coordinate;
// use geo_svg_io::geo_svg_writer::ToSvg;
use geo_svg::ToSvgStr;
use geo_svg::Color;
use geo_svg::ToSvg;
use geo::BoundingRect;
use std::io::Write;

use geo_types::{MultiPolygon, polygon};

const METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR: f64 = 3600.0 / 1000.0;
const TOP_ON_FOOT_SPEED: f64 = 8.0;

fn main() {
    // This XML file actually exists — try it for yourself!
    // let file = File::open("./11-Jul-2022-1827.gpx").unwrap();
    let file = File::open("babbled.gpx").unwrap();
    let reader = BufReader::new(file);

    // read takes any io::Read and gives a Result<Gpx, Error>.
    let mut gpx: Gpx = read(reader).unwrap();

    // Each GPX file has multiple "tracks", this takes the first one.
    // assert_eq!(track.name, Some(String::from("Example GPX Document")));

    // Each track will have different segments full of waypoints, where a
    // waypoint contains info like latitude, longitude, and elevation.

    let mut new_gpx: Gpx = Default::default();
    new_gpx.version = GpxVersion::Gpx11;
    let mut new_track: Track = Track::new();
    let mut new_seg = TrackSegment::new();
    let mut current_state = "nothing";
    // let line_string = LineString(vec![]);
    let mut line_string = gpx.tracks[0].segments[0].linestring();
    let bounding = line_string.bounding_rect();
    line_string = line_string.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding.unwrap().min().x,
            y: coord.y() - bounding.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy: (f64, f64) = (line_string[0].x, line_string[0].y);
    line_string = line_string.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy.0,
            y: coord.y() - startxy.1,
        }
    }).collect();

    let mut line_string2 = gpx.tracks[1].segments[0].linestring();
    let bounding2 = line_string2.bounding_rect();
    line_string2 = line_string2.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding2.unwrap().min().x,
            y: coord.y() - bounding2.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy2: (f64, f64) = (line_string2[0].x, line_string2[0].y);
    line_string2 = line_string2.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy2.0,
            y: coord.y() - startxy2.1,
        }
    }).collect();

    let mut line_string3 = gpx.tracks[2].segments[0].linestring();
    let bounding3 = line_string3.bounding_rect();
    line_string3 = line_string3.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding3.unwrap().min().x,
            y: coord.y() - bounding3.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy3: (f64, f64) = (line_string3[0].x, line_string3[0].y);
    line_string3 = line_string3.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy3.0,
            y: coord.y() - startxy3.1,
        }
    }).collect();

    let mut line_string4 = gpx.tracks[3].segments[0].linestring();
    let bounding4 = line_string4.bounding_rect();
    line_string4 = line_string4.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding4.unwrap().min().x,
            y: coord.y() - bounding4.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy4: (f64, f64) = (line_string4[0].x, line_string4[0].y);
    line_string4 = line_string4.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy4.0,
            y: coord.y() - startxy4.1,
        }
    }).collect();

    let mut line_string5 = gpx.tracks[4].segments[0].linestring();
    let bounding5 = line_string5.bounding_rect();
    line_string5 = line_string5.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding5.unwrap().min().x,
            y: coord.y() - bounding5.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy5: (f64, f64) = (line_string5[0].x, line_string5[0].y);
    line_string5 = line_string5.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy5.0,
            y: coord.y() - startxy5.1,
        }
    }).collect();

    let mut line_string6 = gpx.tracks[4].segments[0].linestring();
    let bounding6 = line_string6.bounding_rect();
    line_string6 = line_string6.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding6.unwrap().min().x,
            y: coord.y() - bounding6.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy6: (f64, f64) = (line_string6[0].x, line_string6[0].y);
    line_string6 = line_string6.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy6.0,
            y: coord.y() - startxy6.1,
        }
    }).collect();

    let mut line_string7 = gpx.tracks[4].segments[0].linestring();
    let bounding7 = line_string7.bounding_rect();
    line_string7 = line_string7.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding7.unwrap().min().x,
            y: coord.y() - bounding7.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy7: (f64, f64) = (line_string7[0].x, line_string7[0].y);
    line_string7 = line_string7.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy7.0,
            y: coord.y() - startxy7.1,
        }
    }).collect();

    let mut line_string8 = gpx.tracks[4].segments[0].linestring();
    let bounding8 = line_string8.bounding_rect();
    line_string8 = line_string8.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - bounding8.unwrap().min().x,
            y: coord.y() - bounding8.unwrap().min().y,
        }
    }).map(|coord| {
        geo::Coordinate {
            x: coord.x * 100000.0,
            y: coord.y * 100000.0,
        }
    }).collect();
    let startxy8: (f64, f64) = (line_string8[0].x, line_string8[0].y);
    line_string8 = line_string8.into_points().iter().map(|coord| {
        geo::Coordinate {
            x: coord.x() - startxy8.0,
            y: coord.y() - startxy8.1,
        }
    }).collect();

    // let polygon = geo_types::Polygon::new(line_string, vec![]);
    let wkt_out = line_string.to_svg()
        .with_stroke_color(Color::Rgb(0, 0, 0))
        .and(line_string2.to_svg().with_stroke_color(Color::Rgb(199, 45,33)))
        .and(line_string3.to_svg().with_stroke_color(Color::Rgb(146, 24, 116)))
        .and(line_string4.to_svg().with_stroke_color(Color::Rgb(72, 33, 135)))
        .and(line_string5.to_svg().with_stroke_color(Color::Rgb(34, 61, 134)))
        .and(line_string6.to_svg().with_stroke_color(Color::Rgb(21, 130,100)))
        .and(line_string7.to_svg().with_stroke_color(Color::Rgb(110, 180, 30)))
        .and(line_string8.to_svg().with_stroke_color(Color::Rgb(199, 194, 33)))
        // .and(line_string8.to_svg().with_stroke_color(Color::Rgb(199, 113, 33)))
        ;
    // println!("{}", wkt_out.to_string());

    let mut newFile2 = BufWriter::new(File::create("image.svg").expect("failed to create test file"));
    // println!("{:#?}", new_gpx);
    newFile2.write_all(wkt_out.to_string().as_bytes());
    // for points in gpx.tracks[0].segments[0].points.iter(){
    //     line_string.
    // }

    for track in gpx.tracks.iter() {
        for segment in track.segments.iter() {
            /*for (pos, points) in segment.points.windows(2).enumerate() {
                // if pos > 2500 {
                //     break;
                // }
                new_seg.points.push(points[1].clone());
                println!("pos {}", pos);
                let point2_speed = speed(&points[0], &points[1]);
                let prev_5min_speed = get_previous_x_minutes_avg_speed(5, &segment.points, pos);
                let next_5_2min_speed = get_next_x_minutes_avg_speed(5, &segment.points, pos);
                let prev_5_2min_speed = get_previous_x_2_minutes_avg_speed(5, &segment.points, pos);
                let next_5min_speed = get_next_x_2_minutes_avg_speed(5, &segment.points, pos);
                if is_on_foot(point2_speed, &next_5min_speed[0..next_5min_speed.len()], &prev_5min_speed[0..prev_5min_speed.len()], &next_5_2min_speed[0..next_5_2min_speed.len()], &prev_5_2min_speed[0..prev_5_2min_speed.len()]) {
                    if current_state != "on_foot" {
                        current_state = "on_foot";
                        if new_seg.points.len() > 50 {
                            new_track.segments.push(new_seg);
                            new_gpx.tracks.push(new_track);
                            new_track = Track::new();
                        }
                        new_seg = TrackSegment::new();
                    }
                    new_seg.points.push(points[1].clone());
                } else {
                    current_state = "nothing";
                }
                println!("{}/{}", pos, segment.points.len());
            }*/
        }
    }

    let mut newFile = BufWriter::new(File::create("test3.gpx").expect("failed to create test file"));
    new_track.segments.push(new_seg);
    new_gpx.tracks.push(new_track);
    // println!("{:#?}", new_gpx);
    write(&new_gpx, newFile).unwrap();
}

/// Calculates distance between 2 points (using pythagorious to calculate height change distance)
/// returns distance in meters
///
fn distance_between_points(point1: geo::Point, ele1: f64, point2: geo::Point, ele2: f64) -> f64 {
    let distance = point1.geodesic_distance(&point2);
    let alt_diff = (ele1 - ele2).abs();
    (distance.powi(2) + alt_diff.powi(2)).sqrt()
}

fn speed(waypoint1: &gpx::Waypoint, waypoint2: &gpx::Waypoint) -> f64 {
    let distance = distance_between_points(
        waypoint1.point(),
        waypoint1.elevation.unwrap(),
        waypoint2.point(),
        waypoint2.elevation.unwrap(),
    );
    let maybe: OffsetDateTime = waypoint1.time.unwrap().into();
    let maybe2: OffsetDateTime = waypoint2.time.unwrap().into();

    let erm = maybe - maybe2;

    let speed_meters_per_second = distance / erm.as_seconds_f64().abs();
    speed_meters_per_second * METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR
}

fn acceleration(waypoint1: &gpx::Waypoint, waypoint2: &gpx::Waypoint, waypoint3: &gpx::Waypoint) -> f64 {
    let speed_difference = speed(waypoint2, waypoint3) - speed(waypoint1, waypoint2);
    // above I'm assuming I calculated the speed I'm going at point 2 and point 3 and point 1 is just unknowable
    // from this I can now wor out how quickly I accelerated from point 2 to point 3
    let maybe2 = OffsetDateTime::parse(&waypoint2.time.unwrap().format().expect("hey"), &Iso8601::DEFAULT).unwrap();
    let maybe3 = OffsetDateTime::parse(&waypoint3.time.unwrap().format().expect("hey"), &Iso8601::DEFAULT).unwrap();
    let time_difference = maybe3 - maybe2;

    let speed_meters_per_second = speed_difference / time_difference.as_seconds_f64().abs();
    speed_meters_per_second
}

/// calculates if point should be considered walking
/// next_5_min 1 f64 for every minute, the further into the array we go the more ahead of time we are
/// prev_5_min 1 f64 for every minute, the further into the array we go the more behind of time we are
fn is_on_foot(speed: f64, next_5_minute_avg_speed: &[Option<f64>], previous_5_minute_avg_speed: &[Option<f64>], next_5_2minute_avg_speed: &[Option<f64>], previous_5_2minute_avg_speed: &[Option<f64>]) -> bool {
    match next_5_minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[1] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[1] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[2] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[2] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_2minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_2minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_2minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_2minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_2minute_avg_speed[2] {
        Some(next_min_speed) => {
            match previous_5_2minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }


    if speed < TOP_ON_FOOT_SPEED {
        return true;
    }

    false
}

fn get_previous_x_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
    }


    for point in points.windows(2).take(current_point_pos).rev() {
        let prev_point_time: OffsetDateTime = point[0].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).floor() as usize;
        if min_diff >= x_min {
            break;
        }
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff].push(speed);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    for i in array_vec.into_iter() {
        if i.len() == 0 {
            result_vec.push(None);
        } else {
            result_vec.push(Some(average(&i[0..i.len()])));
        }
        // println!("MODE: {}", mode(&i));
        // println!("MEDIAN: {}", median(&mut i));
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}


fn get_next_x_minutes_avg_speed_new(x_min: usize, mut point_iter: Peekable<Skip<Windows<'_, Waypoint>>>) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = point_iter.peek().unwrap()[0].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_distance: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_time: Vec<Vec<OffsetDateTime>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
        array_vec_distance.push(Vec::new());
        array_vec_time.push(Vec::new());
    }


    for point in point_iter {
        let prev_point_time: OffsetDateTime = point[1].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).abs().floor() as usize;
        if min_diff >= x_min {
            break;
        }
        array_vec_time[min_diff].push(point[1].time.unwrap().into());
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff].push(speed);

        let distance = distance_between_points(point[0].point(), point[0].elevation.unwrap(), point[1].point(), point[1].elevation.unwrap());
        array_vec_distance[min_diff].push(distance);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    // println!("{:?}", array_vec_distance);

    for (num, i) in array_vec.into_iter().enumerate() {
        if i.len() == 0 {
            result_vec.push(None);
            // println!("opps");
            // println!("{:?}", array_vec_distance[num]);
        } else {
            let distance_sum: f64 = array_vec_distance[num].iter().sum();
            // println!("distance sum {}", distance_sum);
            // println!("distance len {}, time len {}",  array_vec_distance[num].len(), array_vec_time[num].len());
            let erm = array_vec_time[num][0] - array_vec_time[num][array_vec_time[num].len() - 1];
            let seconds = erm.as_seconds_f64().abs();
            if seconds == 0.0 {
                result_vec.push(None); // timestamps are the same bad calculation just skip
            } else {
                let speed_meters_per_second = distance_sum / erm.as_seconds_f64().abs();
                let speed_km_per_hour = speed_meters_per_second * METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
                // if speed_km_per_hour * 1.3 < average(&i[0..i.len()]) || speed_km_per_hour * 0.7 > average(&i[0..i.len()]) {
                //     println!("speed calcs differ {}, {}", speed_km_per_hour, average(&i[0..i.len()]))
                // }
                result_vec.push(Some(speed_km_per_hour));
            }
        }
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}

fn get_next_x_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_distance: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_time: Vec<Vec<OffsetDateTime>> = Vec::new();
    for _ in 0..x_min {
        array_vec.push(Vec::new());
        array_vec_distance.push(Vec::new());
        array_vec_time.push(Vec::new());
    }


    for point in points.windows(2).skip(current_point_pos) {
        let prev_point_time: OffsetDateTime = point[1].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).abs().floor() as usize;
        if min_diff >= x_min {
            break;
        }
        array_vec_time[min_diff].push(point[1].time.unwrap().into());
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff].push(speed);

        let distance = distance_between_points(point[0].point(), point[0].elevation.unwrap(), point[1].point(), point[1].elevation.unwrap());
        array_vec_distance[min_diff].push(distance);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    // println!("{:?}", array_vec_distance);

    for (num, i) in array_vec.into_iter().enumerate() {
        if i.len() == 0 {
            result_vec.push(None);
            // println!("opps");
            // println!("{:?}", array_vec_distance[num]);
        } else {
            let distance_sum: f64 = array_vec_distance[num].iter().sum();
            // println!("distance sum {}", distance_sum);
            // println!("distance len {}, time len {}",  array_vec_distance[num].len(), array_vec_time[num].len());
            let erm = array_vec_time[num][0] - array_vec_time[num][array_vec_time[num].len() - 1];
            let seconds = erm.as_seconds_f64().abs();
            if seconds == 0.0 {
                result_vec.push(None); // timestamps are the same bad calculation just skip
            } else {
                let speed_meters_per_second = distance_sum / erm.as_seconds_f64().abs();
                let speed_km_per_hour = speed_meters_per_second * METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
                // if speed_km_per_hour * 1.3 < average(&i[0..i.len()]) || speed_km_per_hour * 0.7 > average(&i[0..i.len()]) {
                //     println!("speed calcs differ {}, {}", speed_km_per_hour, average(&i[0..i.len()]))
                // }
                result_vec.push(Some(speed_km_per_hour));
            }
        }
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}


fn get_previous_x_2_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
    }


    for point in points.windows(2).take(current_point_pos).rev() {
        let prev_point_time: OffsetDateTime = point[0].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).floor() as usize;
        if min_diff * 2 >= x_min {
            break;
        }
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff / 2].push(speed);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    for i in array_vec.into_iter() {
        if i.len() == 0 {
            result_vec.push(None);
        } else {
            result_vec.push(Some(average(&i[0..i.len()])));
        }
        // println!("MODE: {}", mode(&i));
        // println!("MEDIAN: {}", median(&mut i));
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}

fn get_next_x_2_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_distance: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_time: Vec<Vec<OffsetDateTime>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
        array_vec_distance.push(Vec::new());
        array_vec_time.push(Vec::new());
    }


    for point in points.windows(2).skip(current_point_pos) {
        let prev_point_time: OffsetDateTime = point[1].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).abs().floor() as usize;
        if min_diff * 2 >= x_min {
            break;
        }
        array_vec_time[min_diff / 2].push(point[1].time.unwrap().into());
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff / 2].push(speed);

        let distance = distance_between_points(point[0].point(), point[0].elevation.unwrap(), point[1].point(), point[1].elevation.unwrap());
        array_vec_distance[min_diff / 2].push(distance);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    // println!("{:?}", array_vec_distance);

    for (num, i) in array_vec.into_iter().enumerate() {
        if i.len() == 0 {
            result_vec.push(None);
            // println!("opps");
            // println!("{:?}", array_vec_distance[num]);
        } else {
            let distance_sum: f64 = array_vec_distance[num].iter().sum();
            // println!("distance sum {}", distance_sum);
            // println!("distance len {}, time len {}",  array_vec_distance[num].len(), array_vec_time[num].len());
            let erm = array_vec_time[num][0] - array_vec_time[num][array_vec_time[num].len() - 1];
            let seconds = erm.as_seconds_f64().abs();
            if seconds == 0.0 {
                result_vec.push(None); // timestamps are the same bad calculation just skip
            } else {
                let speed_meters_per_second = distance_sum / erm.as_seconds_f64().abs();
                let speed_km_per_hour = speed_meters_per_second * METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
                // if speed_km_per_hour * 1.3 < average(&i[0..i.len()]) || speed_km_per_hour * 0.7 > average(&i[0..i.len()]) {
                //     println!("speed calcs differ {}, {}", speed_km_per_hour, average(&i[0..i.len()]))
                // }
                result_vec.push(Some(speed_km_per_hour));
            }
        }
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}


fn average(numbers: &[f64]) -> f64 {
    numbers.iter().sum::<f64>() as f64 / numbers.len() as f64
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_load_csv_stock_file() {
        let file = File::open("./test_files/1.gpx").unwrap();
        let reader = BufReader::new(file);

        // read takes any io::Read and gives a Result<Gpx, Error>.
        let mut gpx: Gpx = read(reader).unwrap();
        let erm = get_next_x_minutes_avg_speed(5, &gpx.tracks[0].segments[0].points, 0);
        let erm_new = get_next_x_minutes_avg_speed_new(5, gpx.tracks[0].segments[0].points.windows(2).skip(0).peekable());

        assert_eq!(erm_new[0], Some(107.37588399905206));

        assert_eq!(erm.len(), erm_new.len());
        assert_eq!(erm[0], erm_new[0]);
        assert_eq!(erm[1], erm_new[1]);
        assert_eq!(erm[2], erm_new[2]);
        assert_eq!(erm[3], erm_new[3]);
        assert_eq!(erm[4], erm_new[4]);
    }
}